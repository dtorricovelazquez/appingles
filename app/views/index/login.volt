<div class="col-md-6 col-md-offset-3">

    <h2 class="text-center text-muted">Login</h2>

    {{ content() }}

    {{ form('class': 'form-login') }}

        <input type="hidden" id="token" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}" />

        <div class="col-md-12">
            {{ form.label("email") }}
            {{ form.render("email") }}
        </div>

        <div class="col-md-12">
            {{ form.label("password") }}
            {{ form.render("password") }}
        </div>

        <div class="col-md-12" style="margin-top: 20px">
            {{ form.render("Login") }}
        </div>
    </form>

</div>