<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="user-scalable=no" /> 
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>App</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <?= $this->assets->outputCss() ?>
    </head>
    <body>
        <div class="container">
            <?php if ($this->router->getActionName() != 'login') :?>
                <a id="logout" href="<?php echo $this->url->get("index/logout");?>">Log out</a>
            <?php endif; ?>
            <?= $this->getContent() ?>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <?= $this->assets->outputJs() ?>
    </body>
</html>
