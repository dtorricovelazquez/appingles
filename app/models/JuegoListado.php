<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class JuegoListado extends \Phalcon\Mvc\Model
{
    
    public $id;
    public $ctrl_active;
    public $ctrl_deleted;
    public $created_at;
    public $updated_at;
    
    public $id_empresa;
    public $nombre;
    public $orden;
    
    public function getSource()
    {
        return 'juego_listado';
    }
    
    public function initialize()
    {
        $this->belongsTo('id_juego_curso', 'JuegoCurso', 'id', array('alias' => 'juegoCursoObj'));
    }

}
