<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class Empresas extends \Phalcon\Mvc\Model
{
    
    public $id;
    public $created_at;
    
    public $nombre;
    
    public function getSource()
    {
        return 'empresas';
    }

}
