<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class JuegoListadoDetalle extends \Phalcon\Mvc\Model
{
    
    public $id;
    public $ctrl_active;
    public $ctrl_deleted;
    public $created_at;
    public $updated_at;
    
    public $id_juego_listado;
    public $nombre;
    public $nombre_link;
    
    public function getSource()
    {
        return 'juego_listado_detalle';
    }

    public function initialize()
    {
        $this->belongsTo('id_juego_listado', 'JuegoListado', 'id', array('alias' => 'juegoListadoObj'));
    }
}
