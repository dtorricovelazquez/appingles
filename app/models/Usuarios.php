<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class Usuarios extends \Phalcon\Mvc\Model
{
    
    public $id;
    public $ctrl_active;
    public $ctrl_deleted;
    public $created_at;
    public $updated_at;
    
    public $username;
    public $password;
    public $email;
    public $id_empresa;
    
    public function getSource()
    {
        return 'usuarios';
    }
    
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator()
        );

        return $this->validate($validator);
    }

}
