<?php

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Submit,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\Identical;


class LoginForm extends Form
{
    public function initialize()
    {
        $email = new Text('email', [
            "placeholder" => "Email",
            "class" => "form-control"
        ]);
        $email->addValidators([
            new PresenceOf([
                "message" => "El email es requerido"
            ]),
            new Email([
                "message" => "El email no es válido"
            ])
        ]);
        $email->setLabel("Email");
        $this->add($email);

        $password = new Password("password", [
            "placeholder" => "Password",
            "class" => "form-control"
        ]);
        $password->addValidator(
            new PresenceOf([
                "message" => "El password es requerido"
            ])
        );
        $password->setLabel("Password");
        $this->add($password);

        $this->add(new Submit("Login", [
            "class" => "btn btn-success btn-block color_orange"
        ]));
    }
}






