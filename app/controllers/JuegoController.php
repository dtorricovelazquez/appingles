<?php

class JuegoController extends ControllerBase
{
    
    public function indexAction()
    {
        $this->assets->addCss("css/style.css");
        $juegoCurso = JuegoCurso::find([
            'conditions' => 'ctrl_active = ?0 and ctrl_deleted = ?1 and id_empresa = ?2',
            'bind' => [
                0 => 1,
                1 => 0,
                2 => $this->session->usuario['id_empresa']
            ],
            'order' => 'orden asc'
        ]);
        $this->view->juegoCurso = $juegoCurso;
    }
    
    public function listadoAction($id_juego_curso)
    {
        $this->assets->addCss("css/style.css");
        if (is_numeric($id_juego_curso)) {
            $juegoListado = JuegoListado::find([
                'conditions' => 'ctrl_active = ?0 and ctrl_deleted = ?1 and id_juego_curso = ?2',
                'bind' => [
                    0 => 1,
                    1 => 0,
                    2 => $id_juego_curso
                ],
                'order' => 'orden asc'
            ]);
            
            if (count($juegoListado) > 0) {
                if (isset($juegoListado['0']->juegoCursoObj->id_empresa)) {
                    if ($juegoListado['0']->juegoCursoObj->id_empresa == $this->session->usuario['id_empresa']) {
                        $this->view->juegoListado = $juegoListado;
                    } else {
                         $messageError = 'Se ha producido un error inesperado, consulte con el administrador para ver que ocurre...';
                    }
                } else {
                    $messageError = 'Se ha producido un error inesperado, consulte con el administrador para ver que ocurre..';
                }
            } else {
                $messageError = 'Todavía no está definido este modulo, lo sentimos :-(';
            }
        } else {
            $messageError = 'Se ha producido un error inesperado, consulte con el administrador para ver que ocurre.';
        }
        
        if (!empty($messageError)) {
            $this->view->message = $messageError;
            return $this->view->pick('error');
        }
    }
    
    public function juegoMatchAction($id_juego_listado)
    {
        $this->assets->addCss("css/style.css");
        $this->assets->addCss("css/juego.css");
        $this->assets->addJs("js/juego.js");
        if (is_numeric($id_juego_listado)) {
            $juegoListadoDetalle = JuegoListadoDetalle::find([
                'conditions' => 'ctrl_active = ?0 and ctrl_deleted = ?1 and id_juego_listado = ?2',
                'bind' => [
                    0 => 1,
                    1 => 0,
                    2 => $id_juego_listado
                ]
            ]);
            
            if (count($juegoListadoDetalle) > 0) {
                if (isset($juegoListadoDetalle['0']->juegoListadoObj->juegoCursoObj->id_empresa)) {
                    if ($juegoListadoDetalle['0']->juegoListadoObj->juegoCursoObj->id_empresa == $this->session->usuario['id_empresa']) {
                        $this->view->juegoListadoDetalle = $juegoListadoDetalle;
                        $nombreCarpetaEmpresa = sha1($juegoListadoDetalle['0']->juegoListadoObj->juegoCursoObj->id_empresa .$juegoListadoDetalle['0']->juegoListadoObj->juegoCursoObj->empresaObj->nombre .'matchMemory');
                        $nombreCarpetaListado = sha1($juegoListadoDetalle['0']->juegoListadoObj->id ."matchMemory");
                        $this->view->urlImagen = "/app/img-$nombreCarpetaEmpresa/$nombreCarpetaListado";
                    } else {
                         $messageError = 'Se ha producido un error inesperado, consulte con el administrador para ver que ocurre...';
                    }
                } else {
                    $messageError = 'Se ha producido un error inesperado, consulte con el administrador para ver que ocurre..';
                }
            } else {
                $messageError = 'Todavía no está definido este modulo, lo sentimos :-(';
            }
        } else {
            $messageError = 'Se ha producido un error inesperado, consulte con el administrador para ver que ocurre.';
        }
        
        if (!empty($messageError)) {
            $this->view->message = $messageError;
            return $this->view->pick('error');
        }
    }
}