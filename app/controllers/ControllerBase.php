<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Url;

class ControllerBase extends Controller
{
    public function initialize()
    {
        // no podemos acceder a ninguna página excepto el login si no estamos logeados
        if (!$this->session->has('usuario')) {
            if ($this->router->getActionName() != 'login') {
                $this->response->redirect('index/login');
                return false;
            }
        }
        
        // si vamos a login y ya tenemos la sesion creado nos redirecciona al menu principal
        if ($this->router->getActionName() == 'login' && $this->session->has('usuario')) {
            $this->response->redirect('index');
            return false;
        }
        return true;
    }
}
