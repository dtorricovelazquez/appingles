<?php

class IndexController extends ControllerBase
{
    
    public function indexAction()
    {
        $this->assets->addCss("css/style.css");
    }
    
    public function loginAction()
    {
        $form = new LoginForm();

        if($this->request->getPost())
        {
            if($this->security->checkToken())
            {
                if($form->isValid($this->request->getPost()) == false)
                {
                    foreach($form->getMessages() as $message)
                    {
                        $this->flash->error($message);
                    }
                }
                else
                {
                    $email = $this->request->getPost('email', array('striptags', 'trim', 'email'));
                    $password = $this->request->getPost('password', array('striptags', 'trim'));

                    $usuario = Usuarios::findFirst([
                        'conditions' => 'ctrl_active = ?0 and ctrl_deleted = ?1 and email = ?2',
                        'bind' => [
                            0 => 1,
                            1 => 0,
                            2 => $email
                        ]
                    ]);

                    if($usuario)
                    {
                        if($this->security->checkHash($password, $usuario->password))
                        {
                            $this->session->usuario = [
                                "id" => $usuario->id,
                                "email" => $usuario->email,
                                "id_empresa" => $usuario->id_empresa
                            ];

                            return $this->response->redirect("index");
                        }
                        else
                        {
                            $this->flash->error("El usuario y/o la contraseña son incorrectas");
                        }
                    }
                    else
                    {
                        $this->flash->error("El usuario y/o la contraseña son incorrectas");
                    }
                }
            }
            else
            {
                $this->flash->error("Error csrf");
            }
        }
        $this->assets->addCss("css/style.css");
        $this->view->form = $form;
        
    }
    
    public function logoutAction()
    {
        $this->session->remove("usuario");
        $this->response->redirect("index/login");
    }

}

