$name = '';
$image = '';
$right_solution = 0;
$wrong_solution = 0;

		//start game
		creating_the_game(vocabulary_array());
		
		function inserting_datas_container_names($identifier_for_input,$name_for_input){
			$html = '<div class="col-sm-6">';
				$html += '<p id="' +$identifier_for_input +'" class="name">' +$name_for_input +'</p>';
			$html += '</div>';
			
			// inserting datas
			$("#container_names").append( $html );
		}
		
		function inserting_datas_container_images($identifier_for_input){
			$html = '<div class="col-sm-6">';
				$html += '<img src="' +$folder_of_images +'/' +$identifier_for_input +'.jpg" alt="'+$identifier_for_input+'" id="'+$identifier_for_input+'_image" class="image" />';
			$html += '</div>';
			
			// inserting datas
			$("#container_images").append( $html );
		}
		
		
		function creating_the_game($array_with_datas_for_container){
		
			// inserting images
			$shuffleArray = shuffleArray($array_with_datas_for_container);
			$.each( $shuffleArray, function( key, value ) {
				$identifier_for_input = value['0'];
				$name_for_input = value['1'];
				
				inserting_datas_container_images($identifier_for_input);
			});
			// inserting names
			$shuffleArray = shuffleArray($array_with_datas_for_container);
			$.each( $shuffleArray, function( key, value ) {
				$identifier_for_input = value['0'];
				$name_for_input = value['1'];
				
				inserting_datas_container_names($identifier_for_input,$name_for_input);
			});
			
		}
		
		
		
		function shuffleArray(array) {
			for (var i = array.length - 1; i > 0; i--) {
				var j = Math.floor(Math.random() * (i + 1));
				var temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
			return array;
		}
		
//loading Game

 $(document).ready(function() {

		function check_we_have_the_same_value_in_name_and_image(){
			$returned = 'selected';
			if($name != '' && $image != ''){
				if($name == $image){
					//correct
					show_image_like_desactive_form();
					show_name_like_desactive_form();
					// change_color_div("#96ffb9");
					desactive_click();
					$returned = 'correct';
					$right_solution ++;
					console.log("$right_solution: " +$right_solution);
					the_game_is_finished();
				}
				else{
					//wrong
					change_color_div("#ff9696");
					deleting_wrong_color($name,$image);
					$returned = 'incorrect';
					$wrong_solution ++;
				}
				reset_values();
				
			}
			
			return $returned;			
		}

		function change_color_div($color){
			$( "#" +$image +"_image" ).parent().css( "background-color", $color );
			$( "#" +$name ).parent().css( "background-color", $color );	
		}
		
		function show_image_like_desactive_form(){
			$( "#" +$image +"_image" ).css("opacity","0.2");
			adding_word_in_image();
		}
		
		function adding_word_in_image(){
			$name_cleaned = get_clear_name_by_dirty_name($image);
			$( "#" +$image +"_image" ).parent().append("<p id='word_solution_" +$image +"'>" +$name_cleaned +"</p>");
			
			// style='position: absolute;top: 10px;left: 50%;margin-left: -25px;transform: rotate(-21deg);'
			
			$( "#word_solution_" +$image ).css("position","absolute");
			$( "#word_solution_" +$image ).css("left","50%");
			$( "#word_solution_" +$image ).css("transform","rotate(-21deg)");
			$( "#word_solution_" +$image ).css("font-size","3em");
			
			$height_image = $( "#" +$image +"_image" ).height();
			$half_height_image = $height_image / 2;
			$( "#word_solution_" +$image ).css("top", $half_height_image +"px");
			
			
			$width_text = $( "#word_solution_" +$image ).width();
			$half_width_text = $width_text / 2;
			$( "#word_solution_" +$image ).css("margin-left","-" +$half_width_text +"px");
			
		}
		
		function get_clear_name_by_dirty_name($dirty_name){
			console.log('$image: ' +$image);
			$clear_name = $dirty_name;
			jQuery.each($vocabulary_array, function($index, $row) {
				$dirty_name_array = $row['0'];
				$clear_name_array = $row['1'];
				if($dirty_name_array == $dirty_name){
					$clear_name = $clear_name_array;
				}
			});
			return $clear_name;
			
		}
	

		//the_game_is_finished();
		function the_game_is_finished(){
			//$right_solution = 100;
			if($right_solution >= $vocabulary_array.length ){
				
				//$array_with_images = getArrayGifCelebration();
				$array_with_images = ['CelebracionChampionsRealSociedad_zps9a5e7aa1.gif','a-celebrar-que-metieron-el-gol-leYMz.gif','beso.gif','celebracion.gif','celebracion_2.gif','dance.gif','gif-sentimiento-celebracion-gol-aficionado-real-madrid.gif','gifs-para-comentar-211.gif','giphy.gif','mano.gif'];
				
				$gif_random = getSomeValueRandomForThisArray($array_with_images);		
				//$gif_random = 'beso.gif';
				$src = '../image/celebration/' + $gif_random;
				
				openModal();
				insetImageInModal($src);
				
				
			}
		}
		
		/*
		function getArrayGifCelebration(){
		    $getArrayGifCelebration = new Array();
			$.ajax({
			  url: "../image/celebration/",
			  success: function(data){
				 $(data).find("td > a").each(function(){
					// will loop through 
					$file = String($(this).attr("href"));
					if($file.indexOf('.gif') > 0){
						push.$file);
					}
				 });
			  }
			});
			
			
			return $arrayGifCelebration;
		}
		*/
		
		function insetImageInModal($src){
				var img = document.createElement('img');
				img.src = $src;
				$width_image = 0;
				$height_image = 0;
				$width_screen = $('#mail_Modal').width();
				$height_screen = $('#mail_Modal').height();
				var getting_width_and_height_image = setInterval(function () {
					if (img.naturalWidth) {
						clearInterval(getting_width_and_height_image);
						
						$width_image = img.naturalWidth;
						$height_image = img.naturalHeight;
						
						$width_image_resized = $width_screen * 0.9;
						$height_image_resized = $width_image_resized * ( $height_image / $width_image );
						
						if($height_image_resized > $height_screen){
							// height 100%
							$margin_top = $height_screen * 0.1 / 2;
							$('#mail_Modal').html("<img src='" + $src +"' id='img_victory' height='90%' style='margin-top:" +$margin_top +"px'/>");
							
						}
						else{
							// width 100%
							$margin_top = ( $height_screen / 2 ) - ( $height_image_resized / 2);
							$('#mail_Modal').html("<img src='" + $src +"' id='img_victory' width='90%' style='margin-top:" +$margin_top +"px'/>");
							
						}
					}
				}, 10);
		}
		
		function openModal(){
				$modal = '<div class="modal-backdrop fade in"></div>';
				$modal += '<div class="modal fade in" id="mail_Modal" style="display: block;text-align:center;" aria-hidden="false">';
				$modal += '</div>';
				
				$("body").append($modal);
		}
		
		function getSomeValueRandomForThisArray(Array){
			$length_array = Array.length - 1;
			$key_random = Math.round(Math.random() * $length_array);
			return Array[$key_random];
		}
		
		function show_name_like_desactive_form(){
			$( "#" +$name ).parent().css( "text-decoration", "line-through" );
		}

		function reset_values(){
			$name = '';
			$image = '';
		}
		
		function desactive_click(){
		
			$( "#" +$name  ).addClass( "name_desactive" );
			$( "#" +$name ).removeClass( "name" );
			
			$( "#" +$image +"_image"  ).addClass( "image_desactive" );			
			$( "#" +$image +"_image"  ).removeClass( "image" );
		}
		
		function deleting_wrong_color($name,$image){
		
			setTimeout(function(){
				$( "#" +$name ).parent().css( "background-color", "#F0E4CC" );
				$( "#" +$image +"_image" ).parent().css( "background-color", "#F0E4CC" );
			
			}, 1000);
			
		}

		$( ".name" ).click(function() {
			$is_it_active = this_button_is_active($(this),"name");
			if($is_it_active == 1){
				$name = $id;
				$returned = check_we_have_the_same_value_in_name_and_image();
				html_footer($returned,$(this),'name');
			}			
		});

		$( ".image" ).click(function() {
			$is_it_active = this_button_is_active($(this),"image");
			if($is_it_active == 1){
				$image = $(this).attr("alt");
				check_we_have_the_same_value_in_name_and_image();
				html_footer($returned,$(this),'image');
			}
		});
		
		function html_footer($returned,$this,$image_or_name){
			if($returned == 'correct'  ){
				$("#footer").html('<p style="color:#96ffb9;">CORRECT</p>');
			}
			else if($returned == 'incorrect'){
				$("#footer").html('<p style="color:#ff9696;">WRONG</p>');
			}
			else if($returned == 'selected'){
				$html = '';
				if($image_or_name == 'image'){
					$src = $this.attr('src');
					$html = '<img src="' +$src +'" width="40%"/>';
				}
				else if($image_or_name == 'name'){
					$html = $this.html();
				}
				
				$("#footer").html($html);
				
				$height_footer = $("#footer").height();
				$("#footer_space").height($height_footer);
			}
		}
		
		function this_button_is_active($this,$value_who_decide_if_it_is_active){
			// we decide if it is active or not by the class
			// class == image -> active
			// class == image_desactive -> desactive_click
			// or 
			// class == name -> active
			// class == name_desactive -> desactive
			$id = $this.attr("id");
			$current_class = document.getElementById($id).className;
			$it_is_active = 0;
			if($current_class == $value_who_decide_if_it_is_active){
				$it_is_active = 1;
			}
			
			return $it_is_active;
		}
		
	
 });